
/**
 * Copyright (c) 2014 rxi
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the MIT license. See LICENSE for details.
 */

#include "darr.h"
#include <string.h>

int arr_expand_ (char **d, size_t *n, size_t *a, int memsz) {
  if (*n + 1 > *a) {
    void *ptr;
    int new_a = (*a == 0) ? 1 : *a << 1;
	if (!*d)
		ptr = malloc (new_a * memsz);
	else
		ptr = realloc (*d, new_a * memsz);
    if (ptr == NULL) return -1;
    *d = ptr;
    *a = new_a;
  }
  return 0;
}

int arr_reserve_ (char **d, size_t *n, size_t *a, int memsz, int n_reserve) {
  (void) n;
  if (n_reserve > *a) {
    void *ptr = realloc (*d, n_reserve * memsz);
    if (ptr == NULL) return -1;
    *d = ptr;
    *a = n_reserve;
  }
  return 0;
}


int arr_reserve_po2_ (
  char **d, size_t *n, size_t *a, int memsz, int n_reserve
) {
  int n2 = 1;
  if (n_reserve == 0) return 0;
  while (n2 < n_reserve) n2 <<= 1;
  return arr_reserve_ (d, n, a, memsz, n2);
}


int arr_compact_ (char **d, size_t *n, size_t *a, int memsz) {
	if (*n == 0) {
		free (*d);
		*d = NULL;
		*a = 0;
		return 0;
	} else {
		void *ptr;
		int new_a = *n;
		ptr = realloc (*d, new_a * memsz);
		if (ptr == NULL) return -1;
		*a = new_a;
		*d = ptr;
	}
	return 0;
}


int arr_insert_ (char **d, size_t *n, size_t *a, int memsz, int idx) {
  int err = arr_expand_ (d, n, a, memsz);
  if (err) return err;
  memmove (*d + (idx + 1) * memsz,
          *d + idx * memsz,
          (*n - idx) * memsz);
  return 0;
}

/* int arr_insert_multiple_ (char **d, size_t *n, size_t *a, int memsz, */
/* 				 int idx, int n_insert) { */
/*   int err = arr_reserve_ (d, n, a, memsz, n_insert); */
/*   if (err) return err; */
/*   memmove (*d + (idx + n_insert * memsz), */
/*           *d + idx * memsz, */
/*           (*n - idx) * memsz); */
/*   return 0; */
/* } */

void arr_splice_ (char **d, size_t *n, size_t *a, int memsz,
				 int start, int count) {
  (void) a;
  memmove (*d + start * memsz,
		  *d + (start + count) * memsz,
		  (*n - start - count) * memsz);
}


void arr_swapsplice_ (char **d, size_t *n, size_t *a, int memsz,
					 int start, int count) {
	(void) a;
  memmove (*d + start * memsz,
          *d + (*n - count) * memsz,
          count * memsz);
}


void arr_swap_ (char **d, size_t *n, size_t *a, int memsz, int idx1, int idx2) {
	(void) n; (void) a;
	unsigned char *e_a, *e_b, e_tmp;
	int count;
	if (idx1 == idx2) return;
	e_a = (unsigned char*) *d + idx1 * memsz;
	e_b = (unsigned char*) *d + idx2 * memsz;
	count = memsz;
	while (count--) {
		e_tmp = *e_a;
		*e_a = *e_b;
		*e_b = e_tmp;
		e_a++, e_b++;
	}
}

int arr_insert_extending_ (char **d, size_t *n, size_t *a, int elem_size, int idx,
		void *val) {

    if (idx < 0)
        return -1;

    if (idx > *n)
        idx = *n;

    if (*n >= *a) {
        size_t new_allocated_size = *a == 0 ? 1 : *a * 2;
        char *new_data = realloc (*d, new_allocated_size * elem_size);
        if (!new_data)
            return -1;

        *d = new_data;
        *a = new_allocated_size;
    }

    memmove (*d + (idx + 1) * elem_size, *d + idx * elem_size, (*n - idx) * elem_size);

    memcpy (*d + idx * elem_size, val, elem_size);

    (*n)++;

    return 0;
}

